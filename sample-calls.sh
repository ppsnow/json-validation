#!/usr/bin/env bash

echo -e "Upload valid schema\n"
curl -i http://localhost:8080/schema/valid-schema -d @src/test/resources/jsons/schema/valid.json; echo -e "\n"
echo -e "---------------\n"

echo -e "Trying to upload duplicated schema\n"
curl -i http://localhost:8080/schema/valid-schema -d @src/test/resources/jsons/schema/valid.json; echo -e "\n"
echo -e "---------------\n"

echo -e "Trying to upload invalid schema\n"
curl -i http://localhost:8080/schema/invalid-schema -d @src/test/resources/jsons/schema/invalid.json; echo -e "\n"
echo -e "---------------\n"

echo -e "Fetch uploaded schema\n"
curl -i http://localhost:8080/schema/valid-schema; echo
echo -e "---------------\n"

echo -e "Trying to fetch nonexistent schema\n"
curl -i http://localhost:8080/schema/some-schema; echo
echo -e "---------------\n"


echo -e "Trying to validate valid document with nulls\n"
curl -i http://localhost:8080/validate/valid-schema -d @src/test/resources/jsons/documents/valid-with-nulls.json; echo -e "\n"
echo -e "---------------\n"

echo -e "Trying to validate valid document without nulls\n"
curl -i http://localhost:8080/validate/valid-schema -d @src/test/resources/jsons/documents/valid-without-nulls.json; echo -e "\n"
echo -e "---------------\n"

echo -e "Trying to validate document with invalid json format\n"
curl -i http://localhost:8080/validate/valid-schema -d @src/test/resources/jsons/documents/invalid-format.json; echo -e "\n"
echo -e "---------------\n"

echo -e "Trying to validate document non-compliant with schema\n"
curl -i http://localhost:8080/validate/valid-schema -d @src/test/resources/jsons/documents/invalid-schema.json; echo -e "\n"
echo -e "---------------\n"

echo -e "Trying to validate document with nonexistent schema\n"
curl -i http://localhost:8080/validate/some-schema -d @src/test/resources/jsons/documents/valid-with-nulls.json; echo -e "\n"
echo -e "---------------\n"