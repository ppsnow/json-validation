package com.snowplow

import cats.effect.IO
import cats.effect.kernel.Resource
import com.dimafeng.testcontainers.MongoDBContainer
import org.testcontainers.utility.DockerImageName

object MongoSupport {

  private val mongoDB = "testdb"
  private val mongoCollection = "schemas"
  private val imageName = DockerImageName.parse("mongo:5.0.3")

  def provideContainer(): Resource[IO, RunningMongo] =
    Resource.make(buildAndStartContainer())(runningMongo => IO(runningMongo.container.stop()))

  def mongoConfig(url: String) =
    s"""mongo {
         url = "$url"
         database = "$mongoDB"
         collection = "$mongoCollection"
      }"""

  def cleanupMongo(mongo: RunningMongo): IO[Unit] = IO {
    mongo.container.container
      .execInContainer("mongo", "--eval", s"db=db.getSiblingDB('$mongoDB'); db.$mongoCollection.deleteMany({})", "--quiet")
  }

  private def buildAndStartContainer() = IO {
    val mongoContainer = MongoDBContainer(imageName)
    mongoContainer.start()
    RunningMongo(mongoContainer, s"mongodb://${mongoContainer.container.getContainerIpAddress}:${mongoContainer.container.getMappedPort(27017)}")
  }

  final case class RunningMongo(container: MongoDBContainer, url: String)
}
