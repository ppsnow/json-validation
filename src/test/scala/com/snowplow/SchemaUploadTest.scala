package com.snowplow

import com.snowplow.BaseTest.{FailureResponse, SuccessfulResponse}
import io.circe.Json
import org.http4s.Status
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.circe._

object SchemaUploadTest extends BaseTest {

  integrationTest("Return 201 status when valid json as schema is uploaded") { implicit client =>
    fixture[SuccessfulResponse](
      testSetup = None,
      testedAction = uploadSchemaFrom("jsons/schema/valid.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.Created) and expect {
            body == SuccessfulResponse(
              action = "uploadSchema",
              id = "config-schema",
              status = "success"
            )
          }
      }
    )
  }

  integrationTest("Return 201 status when valid json as schema is uploaded and there is already one but with different id") { implicit client =>
    fixture[SuccessfulResponse](
      testSetup = Some(uploadSchemaFrom("jsons/schema/valid.json", schemaId = "config-schema1")),
      testedAction = uploadSchemaFrom("jsons/schema/valid.json", schemaId = "config-schema2"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.Created) and expect {
            body == SuccessfulResponse(
              action = "uploadSchema",
              id = "config-schema2",
              status = "success"
            )
          }
      }
    )
  }

  integrationTest("After successful upload schema should be fetched successfully") { implicit client =>
    val usedSchema = "jsons/schema/valid.json"

    fixture[Json](
      testSetup = Some(uploadSchemaFrom(usedSchema, schemaId = "config-schema")),
      testedAction = fetchSchema("config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.Ok) and
            expect(body == readJson(usedSchema))
      }
    )
  }

  integrationTest("Return 404 with error description when fetched schema is not uploaded") { implicit client =>
    fixture[FailureResponse](
      testSetup = None,
      testedAction = fetchSchema("config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.NotFound) and expect {
            body == FailureResponse(
              action = "retrieveSchema",
              id = "config-schema",
              status = "error",
              message = "Could not find schema with provided id"
            )
          }
      }
    )
  }

  integrationTest("Return 400 status when invalid json as schema is uploaded") { implicit client =>
    fixture[FailureResponse](
      testSetup = None,
      testedAction = uploadSchemaFrom("jsons/schema/invalid.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.BadRequest) and expect {
            body == FailureResponse(
              action = "uploadSchema",
              id = "config-schema",
              status = "error",
              message = "Provided body is not a valid json"
            )

          }
      }
    )
  }

  integrationTest("Return 409 status and when duplicated schema is uploaded") { implicit client =>
    fixture[FailureResponse](
      testSetup = Some(uploadSchemaFrom("jsons/schema/valid.json", schemaId = "config-schema")),
      testedAction = uploadSchemaFrom("jsons/schema/valid.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.Conflict) and expect {
            body == FailureResponse(
              action = "uploadSchema",
              id = "config-schema",
              status = "error",
              message = "Schema with id: 'config-schema' already exists"
            )
          }
      }
    )
  }
}
