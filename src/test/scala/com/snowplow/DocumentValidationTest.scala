package com.snowplow

import com.snowplow.BaseTest.{FailureResponse, SuccessfulResponse}
import org.http4s.Status
import org.http4s.circe.CirceEntityDecoder._

object DocumentValidationTest extends BaseTest {

  integrationTest("Return 200 status when valid document without null fields is passed") { implicit client =>
    fixture[SuccessfulResponse](
      testSetup = Some(uploadSchemaFrom(path = "jsons/schema/valid.json", schemaId = "config-schema")),
      testedAction = validateJsonFrom(path = "jsons/documents/valid-without-nulls.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.Ok) and expect {
            body == SuccessfulResponse(
              action = "validateDocument",
              id = "config-schema",
              status = "success"
            )
          }
      }
    )
  }

  integrationTest("Return 200 status when valid document with null fields is passed") { implicit client =>
    fixture[SuccessfulResponse](
      testSetup = Some(uploadSchemaFrom(path = "jsons/schema/valid.json", schemaId = "config-schema")),
      testedAction = validateJsonFrom(path = "jsons/documents/valid-with-nulls.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.Ok) and expect {
            body == SuccessfulResponse(
              action = "validateDocument",
              id = "config-schema",
              status = "success"
            )
          }
      }
    )
  }

  integrationTest("Return 400 status when json with invalid format is passed to validation") { implicit client =>
    fixture[FailureResponse](
      testSetup = Some(uploadSchemaFrom(path = "jsons/schema/valid.json", schemaId = "config-schema")),
      testedAction = validateJsonFrom(path = "jsons/documents/invalid-format.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.BadRequest) and expect {
            body == FailureResponse(
              action = "validateDocument",
              id = "config-schema",
              status = "error",
              message = "Provided body is not a valid json"
            )
          }
      }
    )
  }

  integrationTest("Return 422 status when document non-compliant with schema is passed to validation") { implicit client =>
    fixture[FailureResponse](
      testSetup = Some(uploadSchemaFrom(path = "jsons/schema/valid.json", schemaId = "config-schema")),
      testedAction = validateJsonFrom(path = "jsons/documents/invalid-schema.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.UnprocessableEntity) and expect {
            body == FailureResponse(
              action = "validateDocument",
              id = "config-schema",
              status = "error",
              message = "ValidationError(#: 2 schema violations found),ValidationError(#: required key [source] not found),ValidationError(#: required key [destination] not found)"
            )
          }
      }
    )
  }

  integrationTest("Return 404 status when there is no schema uploaded before validation") { implicit client =>
    fixture[FailureResponse](
      testSetup = None,
      testedAction = validateJsonFrom(path = "jsons/documents/valid-with-nulls.json", schemaId = "config-schema"),
      assertions = {
        case (response, body) =>
          expect(response.status == Status.NotFound) and expect {
            body == FailureResponse(
              action = "validateDocument",
              id = "config-schema",
              status = "error",
              message = "Could not find schema with provided id"
            )
          }
      }
    )
  }
}
