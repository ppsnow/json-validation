package com.snowplow

import cats.effect.{IO, Resource}
import com.snowplow.BaseTest.TestResources
import com.snowplow.MongoSupport.{RunningMongo, cleanupMongo, mongoConfig}
import io.circe.generic.semiauto.deriveDecoder
import io.circe.parser.parse
import io.circe.{Decoder, Json}
import org.http4s.Method.{GET, POST}
import org.http4s.blaze.client.BlazeClientBuilder
import org.http4s.client.Client
import org.http4s.client.dsl.io._
import org.http4s.{EntityDecoder, Request, Response, Uri}
import pureconfig.ConfigSource
import weaver.{Expectations, IOSuite}

import scala.io.Source

abstract class BaseTest extends IOSuite {

  override val maxParallelism: Int = 1
  override type Res = TestResources

  override val sharedResource: Resource[IO, Res] = {
    for {
      mongoWithConfig <- createMongoAndAdjustConfig()
      _ <- Factory.buildApp(mongoWithConfig._2)
      client <- BlazeClientBuilder[IO].resource
    } yield TestResources(mongoWithConfig._1, client)
  }

  protected def integrationTest: String => (Client[IO] => IO[Expectations]) => Unit =
    testName =>
      testBody => {
        test(testName) {
          resources =>
            testBody(resources.httpClient)
              .guarantee(cleanupMongo(resources.mongo))
        }
      }

  protected def uploadSchemaFrom(path: String, schemaId: String): Request[IO] = {
    POST(readJsonString(path), Uri.unsafeFromString(s"http://localhost:8080/schema/$schemaId"))
  }

  protected def validateJsonFrom(path: String, schemaId: String): Request[IO] = {
    POST(readJsonString(path), Uri.unsafeFromString(s"http://localhost:8080/validate/$schemaId"))
  }

  protected def fetchSchema(schemaId: String): Request[IO] = {
    GET(Uri.unsafeFromString(s"http://localhost:8080/schema/$schemaId"))
  }

  protected def fixture[BODY](testSetup: Option[Request[IO]],
                              testedAction: Request[IO],
                              assertions: ((Response[IO], BODY)) => Expectations)
                             (implicit client: Client[IO],
                              decoder: EntityDecoder[IO, BODY]): IO[Expectations] = {
    for {
      _ <- testSetup.map(client.run(_).use_).getOrElse(IO.pure(()))
      responseWithBody <- client.run(testedAction).use(response => response.as[BODY].map(body => (response, body)))
    } yield assertions(responseWithBody)
  }

  protected def readJsonString(file: String): String = {
    Source.fromResource(file).mkString
  }

  protected def readJson(file: String): Json = {
    parse(readJsonString(file)).toOption.get
  }

  private def createMongoAndAdjustConfig() = {
    MongoSupport.provideContainer().map { runningMongo =>
      val adjustedMongoConfig = ConfigSource.string(mongoConfig(runningMongo.url))
      val fullConfig = ConfigSource.defaultApplication.withFallback(adjustedMongoConfig)
      (runningMongo, fullConfig)
    }
  }
}

object BaseTest {
  final case class TestResources(mongo: RunningMongo, httpClient: Client[IO])

  final case class ResponseWithBody[A](response: Response[IO], body: A)
  final case class SuccessfulResponse(action: String, id: String, status: String)
  final case class FailureResponse(action: String, id: String, status: String, message: String)

  implicit val successfulResponseDecoder: Decoder[SuccessfulResponse] = deriveDecoder
  implicit val failureResponseDecoder: Decoder[FailureResponse] = deriveDecoder
}