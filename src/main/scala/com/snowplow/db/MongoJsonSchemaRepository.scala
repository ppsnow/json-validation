package com.snowplow.db

import cats.effect.IO
import cats.effect.kernel.Resource
import com.snowplow.AppConfig.MongoConfig
import com.snowplow.core.JsonSchemaRepository
import com.snowplow.core.JsonSchemaRepository.SavingResult
import com.snowplow.core.domain.{JSON, JsonSchema, SchemaId}
import io.circe.{Decoder, Encoder}
import mongo4cats.circe.circeCodecProvider
import mongo4cats.client.MongoClient
import mongo4cats.collection.MongoCollection
import mongo4cats.collection.operations.Filter

class MongoJsonSchemaRepository(collection: MongoCollection[IO, JsonSchema])
  extends JsonSchemaRepository {

  override def saveSchema(schema: JsonSchema): IO[SavingResult] = {
    getSchema(schema.id).flatMap {
      case Some(_) =>
        IO(SavingResult.SchemaAlreadyExists)
      case None =>
        collection.insertOne(schema)
          .map(_ => SavingResult.Success)
    }
  }

  override def getSchema(schemaID: SchemaId): IO[Option[JsonSchema]] = {
    collection
      .find
      .filter(Filter.eq("_id", schemaID.value))
      .first
  }
}

object MongoJsonSchemaRepository {

  def create(config: MongoConfig): Resource[IO, MongoJsonSchemaRepository] = {
    MongoClient.fromConnectionString[IO](config.url)
      .flatMap { client =>
        Resource.eval {
          for {
            db <- client.getDatabase(config.database)
            collection <- db.getCollectionWithCodec[JsonSchema](config.collection)
          } yield new MongoJsonSchemaRepository(collection)
        }
      }
  }

  implicit val decoder: Decoder[JsonSchema] =
    Decoder.forProduct2[JsonSchema, String, JSON]("_id", "representation")((id, representation) => JsonSchema(SchemaId(id), representation))
  implicit val encoder: Encoder[JsonSchema] =
    Encoder.forProduct2[JsonSchema, String, JSON]("_id", "representation")(schema => (schema.id.value, schema.representation))

}