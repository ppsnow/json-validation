package com.snowplow

import cats.effect.{IO, Resource}
import cats.syntax.all._
import com.snowplow.AppConfig.ServerConfig
import com.snowplow.api.{SchemaApi, ValidationApi}
import com.snowplow.core.JsonSchemaRepository
import com.snowplow.db.MongoJsonSchemaRepository
import org.http4s.HttpApp
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Server
import org.http4s.server.middleware.Logger
import pureconfig.ConfigSource

case object App

object Factory {

  def buildApp(configSource: ConfigSource): Resource[IO, App.type] = {
    val config = AppConfig.load(configSource)

    for {
      repository <- buildJsonSchemaRepository(config)
      httpApp = buildHttpApp(repository)
      _ <- buildServer(httpApp, config.server)
    } yield App
  }

  private def buildJsonSchemaRepository(config: AppConfig): Resource[IO, JsonSchemaRepository] = {
    MongoJsonSchemaRepository.create(config.mongo)
  }

  private def buildHttpApp(repository: JsonSchemaRepository) = {
    val schemaApi = new SchemaApi(repository)
    val validationApi = new ValidationApi(repository)
    val httpApp = (schemaApi.routes <+> validationApi.routes).orNotFound
    Logger.httpApp(logHeaders = true, logBody = true)(httpApp)
  }

  private def buildServer(httpApp: HttpApp[IO],
                          config: ServerConfig): Resource[IO, Server] = {
    EmberServerBuilder
      .default[IO]
      .withHost(config.host)
      .withPort(config.port)
      .withHttpApp(httpApp)
      .build
  }
}
