package com.snowplow.core

object domain {

  type JSON = io.circe.Json

  final case class CleanedJson private(value: JSON)

  object CleanedJson {
    def apply(value: JSON): CleanedJson = new CleanedJson(value.deepDropNullValues)
  }

  final case class SchemaId(value: String) extends AnyVal
  final case class JsonSchema(id: SchemaId, representation: JSON)
}
