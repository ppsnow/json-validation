package com.snowplow.core

import cats.data.ValidatedNel
import com.snowplow.core.domain.{CleanedJson, JsonSchema}
import com.typesafe.scalalogging.LazyLogging

object JsonValidator extends LazyLogging {

  final case class ValidationError(message: String)
  type ValidationResult = ValidatedNel[ValidationError, Unit]

  def validateJson(json: CleanedJson, schema: JsonSchema): ValidationResult = {
    logger.info(s"Validating json: '${json.value.noSpaces}' using schema: '${schema.id.value}'")
    io.circe.schema.Schema
      .load(schema.representation)
      .validate(json.value)
      .leftMap(_.map(error => ValidationError(error.getMessage)))
  }
}
