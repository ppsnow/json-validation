package com.snowplow.core

import cats.effect.IO
import com.snowplow.core.JsonSchemaRepository.SavingResult
import com.snowplow.core.domain.{JsonSchema, SchemaId}

trait JsonSchemaRepository {

  def saveSchema(json: JsonSchema): IO[SavingResult]

  def getSchema(schemaID: SchemaId): IO[Option[JsonSchema]]
}

object JsonSchemaRepository {

  sealed trait SavingResult

  object SavingResult {
    case object SchemaAlreadyExists extends SavingResult
    case object Success extends SavingResult
  }
}