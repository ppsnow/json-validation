package com.snowplow

import com.comcast.ip4s.{Host, Port}
import com.snowplow.AppConfig.{MongoConfig, ServerConfig}
import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.auto._

final case class AppConfig(server: ServerConfig, mongo: MongoConfig)

object AppConfig {

  def load(configSource: ConfigSource): AppConfig = configSource.loadOrThrow[AppConfig]

  final case class ServerConfig(host: Host, port: Port)
  final case class MongoConfig(url: String, database: String, collection: String)

  implicit val portReader: ConfigReader[Port] = ConfigReader.fromStringOpt(Port.fromString)
  implicit val hostReader: ConfigReader[Host] = ConfigReader.fromStringOpt(Host.fromString)

}