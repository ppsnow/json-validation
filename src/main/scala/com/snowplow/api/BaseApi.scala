package com.snowplow.api

import cats.effect.IO
import com.snowplow.api.BaseApi.Encoders._
import com.snowplow.api.BaseApi._
import com.snowplow.core.JsonSchemaRepository
import com.snowplow.core.domain.{JsonSchema, SchemaId}
import com.typesafe.scalalogging.LazyLogging
import io.circe.syntax._
import io.circe.{Encoder, Json}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.{Request, Response}

abstract class BaseApi(jsonSchemaRepository: JsonSchemaRepository) extends Http4sDsl[IO] with LazyLogging {

  protected def fetchSchema(schemaId: SchemaIdParam,
                            action: Action)
                           (onFound: JsonSchema => IO[Response[IO]]): IO[Response[IO]] = {
    jsonSchemaRepository.getSchema(SchemaId(schemaId.value)).flatMap {
      case Some(existingSchema) =>
        onFound(existingSchema)
      case None =>
        NotFound(FailedResponse(action, schemaId, "Could not find schema with provided id").asJson)
    }
  }

  protected def readJsonFromRequestBody(request: Request[IO],
                                        schemaId: SchemaIdParam,
                                        action: Action)(onValid: Json => IO[Response[IO]]): IO[Response[IO]] = {
    request.as[Json].attempt.flatMap {
      case Right(validJson) =>
        onValid(validJson)
      case Left(ex) =>
        logger.warn("Invalid json in request body", ex)
        BadRequest(FailedResponse(action, schemaId, "Provided body is not a valid json").asJson)
    }
  }
}

object BaseApi {

  final case class SchemaIdParam(value: String)

  object SchemaIdParam {
    def unapply(str: String): Option[SchemaIdParam] = {
      Some(SchemaIdParam(str))
    }
  }

  final case class SuccessfulResponse(action: Action, id: SchemaIdParam)
  final case class FailedResponse(action: Action, id: SchemaIdParam, message: String)

  sealed trait Action
  object Action {
    case object UploadJsonSchema extends Action
    case object JsonRetrieval extends Action
    case object ValidateJson extends Action
  }

  object Encoders {
    implicit val actionEncoder: Encoder[Action] = Encoder.encodeString.contramap {
      case Action.UploadJsonSchema => "uploadSchema"
      case Action.JsonRetrieval => "retrieveSchema"
      case Action.ValidateJson => "validateDocument"
    }

    implicit val schemaIdEncoder: Encoder[SchemaIdParam] = Encoder.encodeString.contramap(_.value)
    implicit val successfulResponseEncoder: Encoder[SuccessfulResponse] = Encoder.forProduct3("action", "id", "status") { response =>
      (response.action.asJson, response.id.asJson, "success")
    }
    implicit val failedResponseEncoder: Encoder[FailedResponse] = Encoder.forProduct4("action", "id", "status", "message") { response =>
      (response.action.asJson, response.id.asJson, "error", response.message.asJson)
    }
  }
}