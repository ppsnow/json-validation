package com.snowplow.api

import cats.effect.IO
import com.snowplow.api.BaseApi.Encoders._
import com.snowplow.api.BaseApi._
import com.snowplow.core.JsonSchemaRepository
import com.snowplow.core.JsonSchemaRepository.SavingResult
import com.snowplow.core.domain.{JsonSchema, SchemaId}
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.{HttpRoutes, Request}

class SchemaApi(jsonSchemaRepository: JsonSchemaRepository) extends BaseApi(jsonSchemaRepository) {

  val routes: HttpRoutes[IO] = HttpRoutes.of {
    case req@POST -> Root / "schema" / SchemaIdParam(schemaId) =>
      handleJsonSchemaUpload(req, schemaId)
    case GET -> Root / "schema" / SchemaIdParam(schemaId) =>
      fetchSchema(schemaId, Action.JsonRetrieval) { schema =>
        Ok(schema.representation)
      }
  }

  private def handleJsonSchemaUpload(req: Request[IO], schemaId: SchemaIdParam) = {
    readJsonFromRequestBody(req, schemaId, Action.UploadJsonSchema) { validJson =>
      val jsonSchema = JsonSchema(SchemaId(schemaId.value), validJson)
      jsonSchemaRepository.saveSchema(jsonSchema).attempt.flatMap {
        case Right(SavingResult.Success) =>
          Created(SuccessfulResponse(Action.UploadJsonSchema, schemaId).asJson)
        case Right(SavingResult.SchemaAlreadyExists) =>
          Conflict(FailedResponse(Action.UploadJsonSchema, schemaId, s"Schema with id: '${schemaId.value}' already exists").asJson)
        case Left(ex) =>
          logger.error("Exception occurred during saving schema", ex)
          InternalServerError(FailedResponse(Action.UploadJsonSchema, schemaId, "There is an internal problem with schema upload").asJson)
      }
    }
  }
}