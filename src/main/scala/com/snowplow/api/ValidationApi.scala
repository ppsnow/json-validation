package com.snowplow.api

import cats.data.Validated
import cats.effect.IO
import com.snowplow.api.BaseApi.Encoders._
import com.snowplow.api.BaseApi._
import com.snowplow.core.domain.{CleanedJson, JsonSchema}
import com.snowplow.core.{JsonSchemaRepository, JsonValidator}
import io.circe.Json
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.{HttpRoutes, Request}

class ValidationApi(jsonSchemaRepository: JsonSchemaRepository)
  extends BaseApi(jsonSchemaRepository) {

  val routes: HttpRoutes[IO] = HttpRoutes.of {
    case req@POST -> Root / "validate" / SchemaIdParam(schemaId) =>
      handleDocumentValidation(req, schemaId).attempt.flatMap {
        case Right(response) =>
          IO(response)
        case Left(ex) =>
          logger.error("Error during document validation", ex)
          InternalServerError(FailedResponse(Action.ValidateJson, schemaId, "There is an internal problem with document validation").asJson)
      }
  }

  private def handleDocumentValidation(req: Request[IO], schemaId: SchemaIdParam) = {
    fetchSchema(schemaId, Action.ValidateJson) { schema =>
      readJsonFromRequestBody(req, schemaId, Action.ValidateJson) { validJson =>
        validateDocument(schemaId, validJson, schema)
      }
    }
  }

  private def validateDocument(schemaId: SchemaIdParam,
                               validJson: Json,
                               schema: JsonSchema) = {
    JsonValidator.validateJson(CleanedJson(validJson), schema) match {
      case Validated.Valid(()) =>
        Ok(SuccessfulResponse(Action.ValidateJson, schemaId).asJson)
      case Validated.Invalid(errors) =>
        val accumulatedErrors = errors.toList.mkString(",")
        UnprocessableEntity(FailedResponse(Action.ValidateJson, schemaId, accumulatedErrors).asJson)
    }
  }
}