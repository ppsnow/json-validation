
package com.snowplow

import cats.effect.{IO, IOApp}
import pureconfig.ConfigSource

object Boot extends IOApp.Simple {

  private val configSource = ConfigSource.defaultApplication

  def run: IO[Unit] = {
    Factory
      .buildApp(configSource)
      .useForever
  }
}
