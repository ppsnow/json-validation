#!/usr/bin/env bash

echo "Building docker image..."
sbt docker:publishLocal

echo "Launching app with docker-compose..."

docker-compose up -d