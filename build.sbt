val catsEffectV = "3.2.9"
val circeV = "0.14.1"
val circeJsonValidatorV = "0.2.0"
val http4sV = "0.23.6"
val jsonValidatorV = "2.2.14"
val logbackV = "1.2.6"
val mongo4catsV = "0.4.1"
val pureConfigV = "0.17.0"
val scalaLoggingV = "3.9.4"
val testContainersV = "0.39.9"
val weaverV = "0.7.7"

lazy val commonSettings = Seq(
  scalaVersion := "2.13.6",
  organization := "com.snowplow",
  version := "0.1.0",
  resolvers += "jitpack".at("https://jitpack.io"),
  Test / parallelExecution := false
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .settings(extraJavaProperties("json-validation"))
  .settings(
    name := "json-validation",
    testFrameworks += new TestFramework("weaver.framework.CatsEffect"),
    libraryDependencies ++= Seq(
      "org.typelevel"               %% "cats-effect"                  % catsEffectV,
      "io.circe"                    %% "circe-json-schema"            % circeJsonValidatorV,
      "io.circe"                    %% "circe-parser"                 % circeV,
      "org.http4s"                  %% "http4s-circe"                 % http4sV,
      "org.http4s"                  %% "http4s-dsl"                   % http4sV,
      "org.http4s"                  %% "http4s-ember-server"          % http4sV,
      "com.github.java-json-tools"  %  "json-schema-validator"        % jsonValidatorV,
      "ch.qos.logback"              %  "logback-classic"              % logbackV,
      "io.github.kirill5k"          %% "mongo4cats-circe"             % mongo4catsV,
      "io.github.kirill5k"          %% "mongo4cats-core"              % mongo4catsV,
      "com.github.pureconfig"       %% "pureconfig"                   % pureConfigV,
      "com.typesafe.scala-logging"  %% "scala-logging"                % scalaLoggingV,

      "com.dimafeng"                %% "testcontainers-scala-mongodb" % testContainersV % Test,
      "org.http4s"                  %% "http4s-blaze-client"          % http4sV         % Test,
      "com.disneystreaming"         %% "weaver-cats"                  % weaverV         % Test,
    )
  )
  .enablePlugins(JavaAppPackaging, DockerPlugin)

def extraJavaProperties(app: String) =
  bashScriptExtraDefines ++= Seq(
    s"""addJava "-Dconfig.file=/etc/$app/application.conf"""",
    s"""addJava "-Dlogback.configurationFile=/etc/$app/logback.xml""""
  )
