# JSON validation

## Implementation

Project is written entirely in Scala and mainly relies on following libraries/technologies:

* cats effect
* http4s (REST api)
* circe-json-schema (json validation, uses `everit` json schema validator)
* testcontainers (provides MongoDB container for testing)
* weaver (test suites)

## Design

* MongoDB is used as a storage for json schemas.
* Endpoint for uploading schema accepts POST HTTP requests.  
  If there is an attempt of schema's upload with duplicated id, application returns 409 http status.
  If app had to support upserts (create if doesn't exist, replace with new when exists) and preserve idempotency, POST would need to be replaced with PUT.

## Launching app

### With docker

#### Prerequisites

`docker-compose` installed.

#### Steps

Just use script `buildAndLaunch.sh`, which builds docker image of app (using `sbt-native-packager` plugin) and then launches it using provided `docker-compose.yml` definition. 
There is also MongoDB defined in `docker-compose.yml` as it is used as a storage for json schemas.

If app's image is already built there is no need to execute `buildAndLaunch.sh`, calling `docker-compose` directly is sufficient:

```shell
docker-compose up -d
```

### With sbt

Application can run directly from sbt, but it needs some MongoDB instance to be launched beforehand (for example in docker container).

To run app:

```shell
sbt -Dconfig.file=./sample-config/for-sbt/application.conf -Dlogback.configurationFile=./sample-config/for-sbt/logback.xml run
```

By default apps starts and listens on port 8080. Configuration can be adjusted in `application.conf` file in `./sample-config` directory

#### Sample calls

When application is up, we can try to call API with curl:

* Upload schema:
```shell
curl -i http://localhost:8080/schema/config-schema -d @src/test/resources/jsons/schema/valid.json
```

* Fetch schema:
```shell
curl -i http://localhost:8080/schema/config-schema
```

* Validate document:
```shell
curl -i http://localhost:8080/validate/config-schema -d @src/test/resources/jsons/documents/valid-with-nulls.json
```

More calls can be found in `sample-calls.sh`

## Run tests

To run integration tests: 

```shell
sbt test
```
Launches MongoDB in docker container (testcontainers) and application. Then, using http client, executes HTTP calls to exposed by app REST API.